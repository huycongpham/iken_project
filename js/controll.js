$(document).ready(function () {
  var sliderBnr = $("#index #bnr_slider");
  if (sliderBnr.length != 0) {
    $("#index #bnr_slider").slick({
      centerMode: true,
      centerPadding: "calc((100% - 1100px) / 2)",
      slidesToShow: 2,
      arrows: false,
      dots: true,
      responsive: [
        {
          breakpoint: 641,
          settings: {
            centerMode: true,
            centerPadding: "calc((100% - 213px) / 2)",
            slidesToShow: 1,
          },
        },
      ],
    });
  }
  $("#mo_slider").slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    centerMode: true,
    variableWidth: true,
  });
  $("#f_bnrs").slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 2,
    arrows: true,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          arrows: true,
          centerMode: true,
          centerPadding: "2px",
          slidesToShow: 2,
        },
      },
    ],
  });
  var sliderMovie = $("#index #mo_slider");
  if (sliderMovie.length != 0) {
    $("#index #mo_slider").slick({
      centerMode: true,
      centerPadding: "calc((100% - 870px) / 2)",
      slidesToShow: 1,
      arrows: true,
      dots: true,
      responsive: [
        {
          breakpoint: 640,
          settings: {
            centerMode: true,
            centerPadding: "calc((100% - 247px) / 2)",
            slidesToShow: 1,
          },
        },
      ],
    });
  }
  var sliderBnrs = $(".footer #f_bnrs");
  if (sliderBnrs.length != 0) {
    $(".footer #f_bnrs").slick({
      slidesToShow: 4,
      arrows: true,
      dots: false,
      responsive: [
        {
          breakpoint: 640,
          settings: {
            slidesToShow: 2,
          },
        },
      ],
    });
  }
});

var coll = document.getElementsByClassName("collapsible");
var i;
for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function () {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight) {
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  });
}

function toggleMenu() {
  $(".header .menu").toggleClass("toggle");
  $(".header .burger").toggleClass("toggle");
  $(".header .header-sp").toggleClass("toggle");
}
